const headerButton = document.getElementsByClassName('header-button')[0]
const headerLinks = document.getElementsByClassName('header-links')[0]
const scrollButton = document.getElementsByClassName('scroll-button')
const rootElement = document.documentElement

headerButton.addEventListener('click', () => {
  headerLinks.classList.toggle('active')
})

function scrollToTop() {
    window.scrollTo(0, 0);
  }

scrollButton.addEventListener('click', scrollToTop())

